/* "multihash" - Multithreaded sha512 hashing with an stdin/stdout api.
 * Copyright (C) 2019  Moritz Duge <MoritzDuge@kolahilft.de>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 *
 *
 *
 *
 * Usage: multihash [--api] [--thr=X] [-- FILE...]
 *
 *   --api
 *          Ignores [-- FILE...] and takes a hex encoded filename per line via stdin.
 *          E.g.: echo -n 'file.txt' | xxd -p | multihash --api
 *          API mode stdout output (errors are given via stderr):
 *            s: position_of_file_in_stdin calculation_start_unix_timestamp
 *            h: position_of_file_in_stdin hex_sha512_hash
s: 1 1562578000
s: 0 1562578000
h: 0 972073eb8aec5ac4f7c1b883ecbd65fbc76605bdb3141188c2884a76215b9eee6115d9e21e53dcfe88f662d85e8b70b9549ac71efc8abcb889d125394a58b182
s: 2 1562578000
h: 2 ffa2d84eb1bef50ed9ea2531dbd1e89968b9650eb844cc168d845c3efaaf84572b24be63add3f9df8645985b49d7929e019470309a26aa1d29ca4f321ece7fe2
h: 1 97353b2f042f1197407c57980f838038abc140b60a3f1d30ff8c236f46a4759abb5837c50443815b9c8a39dd61b549a1b205eb8752ba65af6982c21f553ea69f
 *
 *   --thr=X
 *          Manually set integer number of threads between 1 and 255.
 *
 *
 * Performance optimization
 *
 * Argument mode: Optimized automatically.
 *
 * API mode:
 * For best performance, place biggest files first. E.g.:
 * multihash -- 20mb 10mb 5mb 4mb 3mb 2mb 1mb
 * Thread 1: 20------------------2-1
 * Thread 2: 10--------5----4----3--
 *
 *
 *
 *
 * Cargo.toml
[package]
name = "multihash"
version = "0.1.0"
edition = "2018"

[dependencies]
hex = "0.3.2"
num_cpus = "1.0"
sha2 = "0.8.0"
 */

use num_cpus;
use sha2::{Sha512, Digest};
use std::cmp;
use std::env;
use std::fs;
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::process;
use std::str;
use std::str::FromStr;
use std::sync::Arc;
use std::sync::atomic::{AtomicUsize, Ordering};
use std::time::SystemTime;
use std::thread;





fn main() {
	let (api_mode, paths, opt_thr) = parse_input();
	hash_path_list(api_mode, paths, opt_thr as usize);
}


// Parses commandline options.
// Also parses STDIN in --api mode.
fn parse_input() -> (bool, Vec<String>, u8) {
	let mut paths_sizes = Vec::with_capacity(env::args().len());
	let mut opt_args = true;
	let mut opt_thr = 0;
	let mut api_mode = false;
	for (i, arg) in env::args().enumerate() {
		if i > 0 {
			if opt_args {
				match arg.as_ref() {
					"--api" => api_mode = true,
					"--" => if !api_mode {
							opt_args = false;
						} else {
								eprintln!("No file arguments in API mode.");
								process::exit(1);
						},
					_ if (&arg[0..6] == "--thr=") => {
							match u8::from_str(&arg[6..]) {
								Err(_) => {},
								Ok(v) => if v != 0 { opt_thr = v; }
							}
							if opt_thr == 0 {
								eprintln!("--thr= needs an integer between 1 and 255. Invalid: {}", &arg[6..]);
								process::exit(1);
							}
						},
					_ => {
						eprintln!("Unknown argument: {}", arg);
						eprintln!("Usage: multihash [--api] [--thr=X] [-- FILE...]");
						process::exit(1);
					}
				}
			} else {
				match fs::metadata(&arg) {
					Err(e) => eprintln!("{:?} => {}", e, arg),
					Ok(v) => {
						// Store filesize (v.len()) and path (arg).
						paths_sizes.push((v.len(), arg));
					}
				}
			}
		}
	}
	// Sort by filesize, to hash big files first.
	paths_sizes.sort_unstable_by(|a, b| b.0.partial_cmp(&a.0).unwrap());
	let mut paths: Vec<String> = paths_sizes.into_iter().map(|x| x.1).collect();

	if api_mode {
		for line in io::stdin().lock().lines() {
			match line {
				Err(e) => eprintln!("{:?}", e),
				Ok(v) => match hex::decode(&v) {
					Err(e) => eprintln!("{:?} => {}", e, v),
					Ok(path) => {
						match str::from_utf8(&path) {
							Err(e) => eprintln!("{:?} => {}", e, v),
							Ok(path_str) => paths.push(String::from(path_str))
						}
					}
				}
			}
		}
	}
	return (api_mode, paths, opt_thr);
}


// Main method for the actual hashing part of the program.
fn hash_path_list(api_mode: bool, paths: Vec<String>, mut opt_thr: usize) {
	let counter = Arc::new(AtomicUsize::new(0));
	let mut children = vec![];
	if opt_thr == 0 {
		opt_thr = cmp::min(num_cpus::get(), paths.len());
	}
	for _ in 1..opt_thr {
		let c_paths = paths.clone();
		let c_counter = counter.clone();
		children.push(thread::spawn(move || hash_worker(api_mode, c_paths, c_counter)));
	}
	hash_worker(api_mode, paths, counter);
	for child in children {
		let _ = child.join();
	}
}


// Opens the next file in path and starts hashing it.
fn hash_worker(api_mode: bool, paths: Vec<String>, counter: Arc<AtomicUsize>) {
	while let Some((path, pos)) = next(&counter, &paths) {
		let file = File::open(&path);
		match file {
			Err(e) => eprintln!("{:?} => {}", e, path),
			Ok(fd) => hash_file(fd, pos, path, api_mode)
		}
	}
}


// Helper to pick the next file from paths by incrementing pos.
fn next(pos: &Arc<AtomicUsize>, paths: &Vec<String>) -> Option<(String, usize)> {
	let i = pos.fetch_add(1, Ordering::SeqCst);
	paths.get(i).map(|path| (path.to_string(), i))
}


// Does the acutal hashing work.
fn hash_file(mut file: File, id: usize, path: String, api_mode: bool) {
	let mut sha512 = Sha512::new();
	match io::copy(&mut file, &mut sha512) {
		Err(e) => println!("{:?} => {}", e, path),
		Ok(_v) => {
			if api_mode {
				let since_epoch = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH);
				println!("s: {} {}", id, since_epoch.unwrap().as_secs());
			}
			let hash = sha512.result();
			if api_mode {
				println!("h: {} {:x}", id, hash);
			} else {
				println!("{:x} {}", hash, path);
			}
		}
	}
}
